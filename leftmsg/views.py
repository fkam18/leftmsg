#  forms.py
#  
#  Copyright 2018 M Kam <sales@eastthing.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
from django.shortcuts import render
from django.utils import timezone
from .models import Msg
from django.http import HttpResponse
from .forms import MsgForm
import uuid
import datetime
from django.conf import settings

# Create your views here.
def msg_clean(request):
	try:
		Msg.objects.filter(created_date__lte=timezone.now()-datetime.timedelta(hours=1)).delete()
		return HttpResponse("done")
	except:
		return HttpResponse("error")
		
def msg_show(request):
	q = request.GET.get('q', '')
	try:
		msg = Msg.objects.get(key=q)
		return render(request, 'leftmsg/msg_show.html', {'msg':msg})
	except Exception as e:
		print(str(e.args[0]))
		return HttpResponse("error: " + str(e.args[0]))

def msg_add(request):
	if request.method == "POST":
		form = MsgForm(request.POST)
		if form.is_valid():
			msg = form.save(commit=False)
			key = str(uuid.uuid4())
			msg.key = key
			msg.created_date = timezone.now()
			msg.save()
			link = settings.URL_HEAD + "/show/?q=" + key
			return render(request, 'leftmsg/msg_add_response.html', {'link': link})
	else:
		form = MsgForm()
	return render(request, 'leftmsg/msg_add.html', {'form': form})
