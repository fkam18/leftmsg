
#  urls.py
#  
#  Copyright 2018 M Kam <sales@eastthing.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from django.urls import path
from . import views
urlpatterns = [
	#path('show', views.msg_show, name='show'),
	#path('add', views.msg_add, name='add'),
	path('show/', views.msg_show, name='msg_show'),
	path('', views.msg_add, name='msg_add'),
	path('clean', views.msg_clean, name='msg_clean'),
]
