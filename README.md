LeftMsg

- Python 3.6.5, Django 2.0.7, virtualenv

Function: allow user to leave a text message, generate an unique URL, 
and pass the URL to 3rd party to retrieve the message, and the message
will be auto deleted after 24 hours with URL removed

